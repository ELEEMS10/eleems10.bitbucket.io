.. _api_oled:

SPI
====

Algemene informatie
*******************
Wat low level SPI functies.


Functies
********
.. doxygenfunction:: spiInitialize
   :project: G2553-Lib

.. doxygenfunction:: spiSendByte
   :project: G2553-Lib

.. doxygenfunction:: spiStop
   :project: G2553-Lib
